Simple Pixel Grid
=================

A lightweight (2.6kb) pixel grid using canvas

See the demo [here](https://roipoussiere.frama.io/simple-pixel-grid)!

## Installation

### With npm

This library is published [as npm module](https://www.npmjs.com/package/simple-pixel-grid):

```bash
npm install --save simple-pixel-grid
```

### With wget

Of course you can simply download the JavaScript file:

```bash
wget https://framagit.org/roipoussiere/simple-pixel-grid/raw/master/dist/pixelGrid.min.js
```

## Quick start

To run the provided example locally:

```bash
npm start
```

Then go to http://127.0.0.1/8008 (if this port is already used, you can define an other with `npm config set simple-pixel-grid:port <port>`)

You should see your pixel grid, that you can draw on it (and erase pixels with right-click):

![](https://framagit.org/roipoussiere/simple-pixel-grid/raw/master/example.png)

## Usage

Initialization: let's suppose that you created a new canvas with the id `pixelGrid`.

```js
var pixelGrid = new PixelGrid(document.getElementById('pixelGrid'), 10, 10);
pixelGrid.init();
```

Draw two pixels, then erase the first one:

```js
pixelGrid.drawPixel({x: 2, y: 4});
pixelGrid.drawPixel({x: 3, y: 4});
pixelGrid.clearPixel({x: 2, y: 4});
```

You can also get a pixel object (`{x, y}`) according to a position on the canvas.

```js
var pixel = this.pixelGrid.getPixelPos(20, 20);
```

This is particulary usefull to draw on the pixel grid with the cursor (see [example](example/main.js)).

## Reference

### PixelGrid(`canvas`, `width`, `height`, `color`, `borderColor`, `borderSize`)

Create a new PixelGrid object.

- **canvas**: the canvas DOM element;
- **width**: the initial pixel grid width, in pixels;
- **height**: the initial pixel grid height, in pixels;
- **color**: the initial pixels colors (default to `#808080`);
- **borderColor**: the initial border color (default to `#404040`);
- **borderSize**: the initial border size (default to 1).

### init()

Actually draw the pixel grid.

### setPixelColor(`color`)

Set a new pixel color.

- **color**: the pixel color (default to `#808080`).

### setGridWidth(`width`)

Set grid width.

- **width**: the new grid width, in pixels.

### setGridHeight(`height`)

Set grid height.

- **height**: the new grid height, in pixels.

### setGridSize(`width`, `height`)

Set grid width and height.

- **width**: the new grid width, in pixels;
- **height**: the new grid height, in pixels.

### drawPixel(`pixel`)

Draw a pixel at the given position.

- **pixel**: a pixel object, as the form `{x: <position on x axis>, y: <position on y axis>}`.

### clearPixel(`pixel`)

Erase a pixel at the given position.

- **pixel**: a pixel object, as the form `{x: <position on x axis>, y: <position on y axis>}`.

### getPixelPos(`clientX`, `clientY`)

Get the pixel position according to a position on the canvas. This is particulary usefull to draw on the pixel grid with the cursor (see [example](example/main.js)).

- **clientX**: a position on the canvas on x axis, in (real) pixels;
- **clientY**: a position on the canvas on y axis, in (real) pixels;
- **returns**: a pixel object, as the form `{x: <grid pixel on x axis>, y: <grid pixel on y axis>}`.

### Licence

![MIT](./LICENCE)
