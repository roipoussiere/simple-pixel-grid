export default class PixelGrid {
  constructor(canvas, width, height, color = '#808080', borderColor = '#404040', borderSize = 1) {
    this.canvas = canvas;
    this.width = width;
    this.height = height;
    this.borderSize = borderSize;

    this.pixels = [...new Array(width)].map(() => new Array(height).fill(false));
    this.ctx = this.canvas.getContext('2d');
    this.ctx.lineWidth = borderSize;
    this.ctx.fillStyle = color;
    this.ctx.strokeStyle = borderColor;

    this.canvas.oncontextmenu = e => e.preventDefault();
  }

  setPixelColor(color = '#808080') {
    this.ctx.fillStyle = color;
  }

  setGridWidth(width) {
    const added = width - this.width;
    if (added > 0) {
      this.pixels.push(...Array.from({ length: added }, () => new Array(this.height).fill(false)));
    } else if (added < 0) {
      this.pixels.splice(added, -added);
    } else {
      return;
    }
    this.width = width;
    this.init();
  }

  setGridHeight(height) {
    const added = height - this.height;
    if (added > 0) {
      this.pixels.map((e) => { e.push(...new Array(added).fill(false)); return e; });
    } else if (added < 0) {
      this.pixels.map((e) => { e.splice(added, -added); return e; });
    } else {
      return;
    }
    this.height = height;
    this.init();
  }

  setGridSize(width, height) {
    this.setGridWidth(width);
    this.setGridHeight(height);
  }

  init() {
    const pixel = {
      width: this.canvas.width / this.width - this.borderSize - this.borderSize / this.width,
      height: this.canvas.height / this.height - this.borderSize - this.borderSize / this.height,
    };
    this.pixSize = Math.min(Math.floor(pixel.width), Math.floor(pixel.height));

    const aliasingOffset = this.borderSize % 2 === 0 ? 0 : 0.5;
    this.offset = {
      x: Math.floor((this.canvas.width - this.scale(this.width)) / 2) + aliasingOffset,
      y: Math.floor((this.canvas.height - this.scale(this.height)) / 2) + aliasingOffset,
    };

    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.drawBorders();
    this.drawGrid();
  }

  drawGrid() {
    for (let x = 0; x < this.width; x += 1) {
      for (let y = 0; y < this.height; y += 1) {
        if (this.pixels[x][y]) {
          this.drawPixel({ x, y });
        }
      }
    }
  }

  scale(xy) {
    return xy * (this.pixSize + this.ctx.lineWidth);
  }

  drawBorders() {
    this.ctx.beginPath();
    for (let x = 0; x < this.width + 1; x += 1) {
      this.ctx.moveTo(this.offset.x + this.scale(x), this.offset.y);
      this.ctx.lineTo(this.offset.x + this.scale(x), this.offset.y + this.scale(this.height));
    }
    for (let y = 0; y < this.height + 1; y += 1) {
      this.ctx.moveTo(this.offset.x, this.offset.y + this.scale(y));
      this.ctx.lineTo(this.offset.x + this.scale(this.width), this.offset.y + this.scale(y));
    }
    this.ctx.stroke();
  }

  drawPixel(pixel) {
    const posX = this.offset.x + this.ctx.lineWidth / 2 + this.scale(pixel.x);
    const posY = this.offset.y + this.ctx.lineWidth / 2 + this.scale(pixel.y);
    this.ctx.fillRect(posX, posY, this.pixSize, this.pixSize);
    this.pixels[pixel.x][pixel.y] = true;
  }

  clearPixel(pixel) {
    const posX = this.offset.x + this.ctx.lineWidth / 2 + this.scale(pixel.x);
    const posY = this.offset.y + this.ctx.lineWidth / 2 + this.scale(pixel.y);
    this.ctx.clearRect(posX, posY, this.pixSize, this.pixSize);
    this.pixels[pixel.x][pixel.y] = false;
  }

  getPixelPos(clientX, clientY) {
    const rect = this.canvas.getBoundingClientRect();
    const pos = { x: clientX - rect.left, y: clientY - rect.top };
    const p = {
      x: Math.floor((pos.x - this.offset.x) / (this.pixSize + this.ctx.lineWidth)),
      y: Math.floor((pos.y - this.offset.y) / (this.pixSize + this.ctx.lineWidth)),
    };
    return p.x >= 0 && p.x < this.width && p.y >= 0 && p.y < this.height ? p : false;
  }
}
