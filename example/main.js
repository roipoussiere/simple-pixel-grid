import PixelGrid from './pixelGrid.min.js';

const gridWidth = document.getElementById('gridWidth').valueAsNumber;
const gridHeight = document.getElementById('gridHeight').valueAsNumber;
const pixelGrid = new PixelGrid(document.getElementById('pixelGrid'), gridWidth, gridHeight);

pixelGrid.init();

function draw(event) {
  const pixel = pixelGrid.getPixelPos(event.clientX, event.clientY);
  if (event.buttons === 1 && pixel && !pixelGrid.pixels[pixel.x][pixel.y]) {
    pixelGrid.drawPixel(pixel);
  } else if (event.buttons === 2 && pixel && pixelGrid.pixels[pixel.x][pixel.y]) {
    pixelGrid.clearPixel(pixel);
  }
}

pixelGrid.canvas.addEventListener('mousedown', event => draw(event));
pixelGrid.canvas.addEventListener('mousemove', event => draw(event));

document.getElementById('gridWidth').addEventListener('input', (event) => {
  if (!event.target.checkValidity()) {
    event.target.classList.add('warning');
    event.target.value = gridWidth;
  } else if (event.target.value !== '') {
    event.target.classList.remove('warning');
    pixelGrid.setGridWidth(event.target.valueAsNumber);
  }
});

document.getElementById('gridHeight').addEventListener('input', (event) => {
  if (!event.target.checkValidity()) {
    event.target.classList.add('warning');
    event.target.value = gridHeight;
  } else if (event.target.value !== '') {
    event.target.classList.remove('warning');
    pixelGrid.setGridHeight(event.target.valueAsNumber);
  }
});
